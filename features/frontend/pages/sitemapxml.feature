Feature: Sitemap.xml
  In order to know what pages to index
  As a search bot
  There should be a sitemap.xml

  @api
  Scenario: Check whether the sitemap.xml is accessible and valid
    Given "article" content:
      | title          |
      | First article  |
      | Second article |
    And the sitemap.xml is up-to-date
    When I am on "sitemap.xml"
    Then I should get a "200" HTTP response
    Then I should get a valid sitemap.xml response
