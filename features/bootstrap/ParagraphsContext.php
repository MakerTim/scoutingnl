<?php

declare(strict_types=1);

use Behat\Gherkin\Node\TableNode;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\DrupalExtension\Context\RawDrupalContext;
use Drupal\DrupalExtension\Hook\Scope\EntityScope;
use Drupal\node\Entity\Node;
use Drupal\node\NodeInterface;
use Drupal\paragraphs\ParagraphInterface;

/**
 * Provides step-definitions for interacting with Drupal paragraphs.
 */
final class ParagraphsContext extends RawDrupalContext {

  protected EntityStorageInterface $paragraphStorage;

  /**
   * Keep track of paragraphs so they can be cleaned up.
   *
   * @var array
   */
  protected array $paragraphs = [];

  protected ?stdClass $lastNode = NULL;

  /**
   * Initializes the context for a single scenario.
   */
  public function __construct() {
    $this->paragraphStorage = \Drupal::entityTypeManager()->getStorage('paragraph');
  }

  /**
   * Remove any created paragraphs.
   *
   * @AfterScenario
   */
  public function cleanParagraphs(): void {
    $this->paragraphStorage->delete($this->paragraphs);
    $this->paragraphs = [];
  }

  public function paragraphCreate(array $values): ParagraphInterface {
    /** @var \Drupal\paragraphs\ParagraphInterface $paragraph */
    $paragraph = $this->paragraphStorage->create($values);
    $paragraph->save();

    $this->paragraphs[$paragraph->id()] = $paragraph;

    return $paragraph;
  }

  /**
   * Keeps track of the last created/visited node.
   *
   * @afterNodeCreate
   */
  public function afterNodeCreate(EntityScope $scope): void {
    $node = $scope->getEntity();
    assert($node instanceof stdClass);
    $this->lastNode = $node;
  }

  /**
   * Adds a paragraph of given type to given field of the last viewed content.
   *
   * The paragraph should be provided in the form:
   * | title     | My paragraph   |
   * | Field One | My field value |
   * | status    | 1              |
   * | ...       | ...            |
   *
   * @throws \Exception
   *
   * @Given the :field contains a :type( paragraph):
   * @Given the :field contains a :type( paragraph)
   */
  public function theFieldContainsAParagraph(string $field, string $type, TableNode $fields = null): void {
    if (!isset($this->lastNode)) {
      throw new RuntimeException('No last node available.');
    }

    $node = Node::load($this->lastNode->nid);

    if ($node instanceof NodeInterface === FALSE) {
      throw new RuntimeException('Could not load node.');
    }

    $paragraph = (object) [
      'type' => $type,
    ];
    if ($fields) {
      foreach ($fields->getRowsHash() as $field_name => $field_value) {
        $paragraph->{$field_name} = $field_value;
      }

      $this->parseEntityFields('paragraph', $paragraph);
      $this->expandEntityFields('paragraph', $paragraph);
    }
    // Cast object to array.
    $values = json_encode($paragraph);
    assert(is_string($values));
    $values = json_decode($values, true);
    assert(is_array($values));

    $saved = $this->paragraphCreate($values);

    $node->get($field)->appendItem($saved);
    $node->save();

    // Set internal browser on the node.
    $this->getSession()->visit($this->locatePath('/node/' . $node->id()));
  }

  protected function expandEntityFields(string $entity_type, \stdClass $entity): void {
    $driver = $this->getDriver();
    $core = $driver->getCore();

    $field_types = $core->getEntityFieldTypes($entity_type);
    foreach ($field_types as $field_name => $type) {
      if (isset($entity->$field_name)) {
        $entity->$field_name = $core->getFieldHandler($entity, $entity_type, $field_name)
          ->expand($entity->$field_name);
      }
    }
  }

}
