<?php

declare(strict_types=1);

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\State\StateInterface;
use Drupal\DrupalExtension\Context\RawDrupalContext;

/**
 * Provides step-definitions for testing emails sent by Drupal.
 */
final class EmailContext extends RawDrupalContext {

  private readonly StateInterface $state;

  private readonly ConfigFactoryInterface $config;

  private mixed $originalMailBackend;

  private mixed $originalMailsystemDefaults;

  private array $lastMail;

  /**
   * Initializes context for a single scenario.
   */
  public function __construct() {
    $container = \Drupal::getContainer();
    $this->config = $container->get('config.factory');
    $this->state = $container->get('state');
  }

  /**
   * @BeforeScenario @email
   */
  public function activateMailCollector(): void {
    $mail_config = $this->config->getEditable('system.mail');
    $mailsystem_config = $this->config->getEditable('mailsystem.settings');

    // Keep track of the original mail backend.
    $this->originalMailBackend = $mail_config->get('interface.default');
    $this->originalMailsystemDefaults = $mailsystem_config->get('defaults.sender');

    // Activate the mail collector backend.
    $mail_config->set('interface.default', 'test_mail_collector')->save();
    $mailsystem_config->set('defaults.sender', 'test_mail_collector')->save();

    $this->resetMailCollector();
  }

  /**
   * @AfterScenario @email
   */
  public function deactivateMailCollector(): void {
    // Restore the original mail backend.
    $this->config
      ->getEditable('system.mail')
      ->set('interface.default', $this->originalMailBackend)
      ->save();

    $this->config
      ->getEditable('mailsystem.settings')
      ->set('defaults.sender', $this->originalMailsystemDefaults)
      ->save();

    $this->resetMailCollector();
  }

  /**
   * Reset the state variable that holds sent messages.
   */
  public function resetMailCollector(): void {
    $this->state->set('system.test_mail_collector', []);
  }

  /**
   * Gets all emails sent during a scenario.
   *
   * @param array $filter
   *   An array containing key/value pairs used to filter the emails that are
   *   returned.
   *
   * @return array
   *   An array containing email messages captured during the current scenario.
   */
  public function getMails(array $filter = []): array {
    // Clear state cache.
    $this->state->resetCache();

    $captured_emails = $this->state->get('system.test_mail_collector', []);
    assert(is_array($captured_emails));
    $filtered_emails = [];

    foreach ($captured_emails as $message) {
      foreach ($filter as $key => $value) {
        if (!isset($message[$key]) || $message[$key] != $value) {
          continue 2;
        }
      }
      $filtered_emails[] = $message;
    }

    return $filtered_emails;
  }

  public function getLastMail(): array {
    if (!$this->lastMail) {
      throw new \Exception('No email being tracked');
    }

    return $this->lastMail;
  }

  /**
   * @Then print sent emails
   */
  public function printSentEmails(): void {
    $mails = $this->getMails();

    foreach ($mails as $mail) {
      echo $mail['subject'] . ' to ' . $mail['to'] . "\n";
    }
  }

  /**
   * Checks an email has been sent to a given recipient.
   *
   * @throws \Exception
   *
   * @Then an email should be sent to :recipient
   */
  public function anEmailShouldBeSentTo(string $recipient): void {
    $mails = $this->getMails(['to' => $recipient]);

    if (empty($mails)) {
      throw new \Exception('No email was sent to ' . $recipient);
    }

    $this->lastMail = reset($mails);
  }

  /**
   * Compares a property of the last checked mail.
   *
   * @throws \Exception
   *
   * @Then the :property of the email :comparison :value
   */
  public function thePropertyOfTheEmailComparison(string $property, string $comparison, string $value): void {
    $mail = $this->getLastMail();

    if (!isset($mail[$property]) || !is_string($mail[$property])) {
      throw new \Exception(sprintf('The %s does not exist', $property));
    }

    // Normalize whitespace, as we don't know what the mail system might have
    // done. Any run of whitespace becomes a single space.
    $normalized = preg_replace('/\s+/', ' ', $mail[$property]);
    assert(is_string($normalized));

    switch ($comparison) {
      case 'is':
      case 'equals':
        if ($normalized != $value) {
          throw new \Exception(sprintf('The %s does not equal %s', $property, $value));
        }
        break;
      case 'contains':
        if (!str_contains($normalized, $value)) {
          throw new \Exception(sprintf('The %s does not contain %s', $property, $value));
        }
        break;

      default:
        throw new \RuntimeException(sprintf('Unknown comparison method %s', $comparison));
    }
  }
}
