<?php

declare(strict_types=1);

use Behat\Mink\Exception\ExpectationException;
use Behat\MinkExtension\Context\RawMinkContext;

/**
 * Provides step-definitions for testing xml responses.
 */
final class XmlContext extends RawMinkContext {

  /**
   * @Then I should get a valid sitemap.xml response
   */
  public function assertValidXml(): void {
    $xml = new DOMDocument();
    $xml->loadXML($this->getSession()->getPage()->getContent());

    $valid = $xml->schemaValidate(__DIR__ . '/../schema/sitemap-xhtml.xsd');

    if (!$valid) {
      throw new ExpectationException('Returned xml does not match schema', $this->getSession()->getDriver());
    }
  }
}
