<?php

declare(strict_types=1);

use Behat\Gherkin\Node\TableNode;
use Drupal\Driver\DrushDriver;
use Drupal\DrupalExtension\Context\RawDrupalContext;

/**
 * Defines application features from the specific context.
 */
final class FeatureContext extends RawDrupalContext {

  /**
   * Initializes context.
   *
   * Every scenario gets its own context instance.
   * You can also pass arbitrary arguments to the
   * context constructor through behat.yml.
   */
  public function __construct() {
  }

  /**
   * @Given :type content with author :author:
   */
  public function createNodeWithAuthor(string $type, string $author, TableNode $nodesTable): void {
    $user = user_load_by_name($author);
    if (!$user) {
      throw new \Exception(sprintf('User %s does not exist', $author));
    }
    foreach ($nodesTable->getHash() as $nodeHash) {
      $node = (object) $nodeHash;
      $node->type = $type;
      $node->uid = $user->id();
      $this->nodeCreate($node);
    }
  }

  /**
   * @Given I am on the edit form of (the ):type (content )(with title ):title
   */
  public function iAmOnTheEditFormOfTheTypeContentWithTitle(string $type, string $title): void {
    $results = \Drupal::entityQuery('node')
      ->accessCheck(FALSE)
      ->condition('type', $type)
      ->condition('title', $title)
      ->execute();
    if (empty($results)) {
      throw new \Exception(sprintf('No content of type %s and title %sexists.', $type, $title));
    }

    $nid = reset($results);

    $this->getSession()->visit($this->locatePath('/node/' . $nid . '/edit'));
  }

  /**
   * @Given /^I wait (\d+) seconds$/
   * @Given /^this took (\d+) seconds$/
   * @Given /^this took at least (\d+) seconds$/
   */
  public function iWaitSeconds(int $wait): void {
    sleep($wait);
  }

  /**
   * @Given /^the sitemap.xml is up-to-date$/
   */
  public function sitemapXml(): void {
    $driver = $this->getDrupal()->getDriver('drush');
    assert($driver instanceof DrushDriver);
    $driver->drush('simple-sitemap:generate');
  }

}
