#!/usr/bin/env bash

main() {
  echo "Running post install commands..."
  echo "COMPOSER_DEV_MODE=$COMPOSER_DEV_MODE"

  if [ "$COMPOSER_DEV_MODE" == "1" ]; then
    bash scripts/sh/check-symlinks.sh
    bash scripts/sh/publish-assets.sh
  fi

  echo "Done"
}

(
  set -euo pipefail
  main
)
