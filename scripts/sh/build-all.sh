#!/usr/bin/env bash

main() {
  echo "Build all the base layers."
  for filename in ./resources/docker/*-*; do
    (cd "${filename}" && docker build . -t "makertim/${filename##*/}")
  done

  echo "Build the application."
  COMPOSE_PROFILES=push-targets docker compose -f docker-compose-fluo.yml build
  echo "OK"
}

(
  set -euo pipefail
  main
)
