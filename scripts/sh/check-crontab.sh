#!/usr/bin/env bash


# Quotes a string for use in a sed regex.
#
# https://stackoverflow.com/questions/29613304/is-it-possible-to-escape-regex-metacharacters-reliably-with-sed
quote-regex() {
  sed -e 's/[^^]/[&]/g; s/\^/\\^/g; $!a\'$'\n''\\n' <<<"$1" | tr -d '\n';
}

quote-substr() {
  IFS= read -d '' -r < <(sed -e ':a' -e '$!{N;ba' -e '}' -e 's/[&/\]/\\&/g; s/\n/\\&/g' <<<"$1")
  printf %s "${REPLY%$'\n'}"
}

boundary-start() {
  local src=$1

  echo "## FILE $src START ##"
}

boundary-end() {
  local src=$1

  echo "## FILE $src END ##"
}

is-installed() {
  local src=$1
  local dest=$2

  local start; start=$(boundary-start "$src")

  grep -q "^$start$" "$dest"
}

install() {
  local src=$1
  local dest=$2

  local start; start=$(boundary-start "$src")
  local end; end=$(boundary-end "$src")
  local data; data=$(<"$src")

  data=${data//\$\{PROJECT_ROOT\}/"$(pwd)"}

  echo -e "$start\n$data\n\n$end" >> "$dest"
}

update() {
  local src=$1
  local dest=$2

  local start; start=$(quote-regex "$(boundary-start "$src")")
  local end; end=$(quote-regex "$(boundary-end "$src")")
  local data; data=$(<"$src")

  data=${data//\$\{PROJECT_ROOT\}/"$(pwd)"}
  data=$(quote-substr "$data")

  # http://fahdshariff.blogspot.com/2012/12/sed-mutli-line-replacement-between-two.html
  sed -i -n "/$start/{p;:a;N;/$end/!ba;s/.*\n/$data\n\n/};p" "$dest"
}

crontab-ensure() {
  if ! crontab -l > /dev/null 2>&1; then
    (
      export VISUAL="ex -c 's/$/\r/|sleep 1|wq!' > /dev/null"
      crontab -e
    )
  fi
}

crontab-include() {
  local src=$1
  local dest=$2

  if [ ! -f "$src" ]; then
    echo "Missing src $src"
    return 1
  fi

  if ! is-installed "$src" "$dest"; then
    install "$src" "$dest"
  else
    update "$src" "$dest"
  fi
}

main() {
  echo "Checking project crontab..."

  crontab-ensure

  local src="$(pwd)/scripts/cron/crontab.tpl"
  local dest; dest=$(mktemp)

  crontab -l > "$dest"
  crontab-include "$src" "$dest"
  crontab "$dest"

  echo "OK"
}

(
  set -euo pipefail
  main
)
