/* global module, require */

const del = require('del');

module.exports = function (config) {
  return function clean() {
    return del([
      config.path.ASSETS_DST,
      config.path.FAVICONS_DST,
      config.path.LIBRARIES_DST
    ]);
  };
};
