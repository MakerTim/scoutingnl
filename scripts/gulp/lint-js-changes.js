/* global module, require */

const gulp = require('gulp');
const gitmodified = require('gulp-gitmodified');
const eslint = require('gulp-eslint');

module.exports = function (config) {
  return function lintJsChanges() {
    return gulp.src(config.path.ASSETS_SRC + '/**/*.js')
      .pipe(gitmodified(['added', 'modified']))
      .pipe(eslint())
      .pipe(eslint.format())
      .pipe(eslint.failAfterError());
  };
};
