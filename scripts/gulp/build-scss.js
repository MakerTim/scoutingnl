/* global module, require */

const gulp = require('gulp');
const gulpif = require('gulp-if');
const sass = require('gulp-sass')(require('sass-embedded'));
const sassGlob = require('gulp-sass-glob');
const sassUnicode = require('gulp-sass-unicode');
const postcss = require('gulp-postcss');
const sortMediaQueries = require('postcss-sort-media-queries');
const autoprefixer = require('autoprefixer');
const precalculateCalc = require("postcss-calc");
const sourcemaps = require('gulp-sourcemaps');
const cleanCss = require('gulp-clean-css');
const rename = require('gulp-rename');

module.exports = function (config) {
  return function buildScss() {
    return gulp.src(config.path.ASSETS_SRC + '/**/*.scss')
      .pipe(sassGlob())
      .pipe(sass({
        outputStyle: 'expanded',
        precision: 10
      }))
      .pipe(sassUnicode())
      .pipe(postcss([
        sortMediaQueries({
          sort: 'mobile-first'
        }),
        autoprefixer({
          cascade: false
        }),
        precalculateCalc()
      ]))
      .pipe(gulp.dest(config.path.ASSETS_DST))
      .pipe(sourcemaps.init())
      .pipe(cleanCss(
        {
          level: 1,
          format: 'keep-breaks',
          inline: ['none'],
          rebase: false
        },
        logCleanCssResults
      ))
      .pipe(rename({suffix: '.min'}))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest(config.path.ASSETS_DST));
  };

  function logCleanCssResults(details) {
    let efficiency = !isNaN(details.stats.efficiency)
      ? Math.round(details.stats.efficiency * 100)
      : 0;

    console.log(
      details.name + ': ' +
      details.stats.originalSize + ' -> ' + details.stats.minifiedSize,
      '(' + efficiency + '%)'
    );
  }
};
