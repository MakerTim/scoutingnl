/* global module, require */

const gulp = require('gulp');
const gulpif = require('gulp-if');
const isBinary = require('gulp-is-binary');
const merge = require('merge-stream');
const lec = require('gulp-line-ending-corrector');

function isNotBinary(file) {
  return !file.isBinary()
}

module.exports = function (config) {
  return function buildLibraries(done) {
    let libraries = config.libraries ? Object.entries(config.libraries) : [];
    if (!libraries.length) {
      return done();
    }

    let tasks = libraries.map(([name, files]) => {
      return gulp.src(files)
        .pipe(isBinary())
        .pipe(gulpif(isNotBinary, lec()))
        .pipe(gulp.dest(config.path.LIBRARIES_DST + '/' + name));
    });

    return merge(tasks);
  };
};
