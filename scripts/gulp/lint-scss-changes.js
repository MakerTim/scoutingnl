/* global module, require */

const gulp = require('gulp');
const gitmodified = require('gulp-gitmodified');
const stylelint = require('gulp-stylelint');

module.exports = function (config) {
  return function lintScssChanges() {
    return gulp.src(config.path.ASSETS_SRC + '/**/*.scss')
      .pipe(gitmodified(['added', 'modified']))
      .pipe(stylelint({
        syntax: 'scss',
        reporters: [
          {formatter: 'string', console: true}
        ]
      }))
  };
};
