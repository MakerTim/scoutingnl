/* global module, require */

const gulp = require('gulp');

module.exports = function (config) {
  return function watch() {
    gulp.watch(config.path.ASSETS_SRC + '/**/*.scss', gulp.parallel('build-scss'));
    gulp.watch(config.path.ASSETS_SRC + '/**/*.js', gulp.parallel('build-js'));
    gulp.watch(config.path.ASSETS_SRC + '/**/*.@(gif|jpg|png|svg)', gulp.parallel('build-images'));
  };
};
