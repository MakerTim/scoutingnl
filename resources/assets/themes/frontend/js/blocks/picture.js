import Drupal from 'drupal';

if ('objectFit' in document.documentElement.style === false) {
  Drupal.behaviors.frontendPicture = {
    attach: context => {
      context.querySelectorAll('picture > img').forEach(el => {
        updateBackgroundImage.call(el);
        el.addEventListener('load', updateBackgroundImage);
        el.style.opacity = 0;
      });
    }
  };
}

function updateBackgroundImage() {
  if (this.complete) {
    this.parentElement.style.backgroundImage = 'url(' + (this.currentSrc || this.src) + ')';
  }
}
