window.addEventListener('hashchange', checkStateSearch);
window.addEventListener('DOMContentLoaded', checkStateSearch);

function checkStateSearch() {
  if (window.location.hash === '#state-search') {
    setTimeout(focusSearch, 0);
  }
}

function focusSearch() {
  const el = document.querySelector('.block-sitesearch input[type=text]');
  if (el) {
    el.focus();
  }
}
