import Headroom from 'headroom.js';

const options = {
  offset: 48,
  tolerance: 12,
  classes: {
    initial: 'page-header--pinnable',
    pinned: 'page-header--pinned',
    unpinned: 'page-header--unpinned',
    top: 'page-header--top',
    notTop: 'page-header--not-top'
  }
};

const el = document.querySelector('.page-header');
if (el) {
  const headroom = new Headroom(el, options);
  headroom.init();
}
