<?php

namespace Drupal\custom_base\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Constraint(
 *   id = "CustomBaseHtmlConstraint",
 *   label = @Translation("Valid HTML", context = "Validation"),
 *   type = { "entity" },
 * )
 */
final class HtmlConstraint extends Constraint {

  public string $messageInvalidTag = '<%tag> tag is not allowed.';

}
