<?php

namespace Drupal\custom_admin;

use Drupal\Core\Breadcrumb\Breadcrumb;
use Drupal\Core\Breadcrumb\BreadcrumbBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

final class BreadcrumbBuilder implements BreadcrumbBuilderInterface {
  use StringTranslationTrait;

  public function applies(RouteMatchInterface $route_match): bool {
    $route_name = $route_match->getRouteName();
    return in_array($route_name, [
      'entity.node.delete_form',
      'entity.node.edit_form',
      'entity.node.version_history',
      'entity.node.content_translation_overview',
      'entity.node.content_translation_add',
      'entity.node.content_translation_edit',
      'entity.node.content_translation_delete',
    ]);
  }

  public function build(RouteMatchInterface $route_match): Breadcrumb {
    $breadcrumb = new Breadcrumb();

    $breadcrumb->addCacheContexts(['route.name']);

    $links[] = Link::createFromRoute($this->t('Home'), '<front>');
    $links[] = Link::createFromRoute($this->t('Administration'), 'system.admin');
    $links[] = Link::createFromRoute($this->t('Content'), 'system.admin_content');

    return $breadcrumb->setLinks($links);
  }

}
