<?php

use Drupal\views\ViewExecutable;

/**
 * Implements hook_views_query_substitutions().
 */
function custom_subsite_views_query_substitutions(ViewExecutable $view): array {
  $account = \Drupal::currentUser();
  $subsite = $account->id() === 0
    ? '%'
    : \Drupal::service('custom_subsite.subsite_repository')
      ->getCurrentSubsiteFor($account)
      ->id();

  return [
    '***SUBSITE_SUPERADMIN***' => intval($account->hasPermission('manager of subsites')),
    '***CURRENT_SUBSITE***' => $subsite,
  ];
}
