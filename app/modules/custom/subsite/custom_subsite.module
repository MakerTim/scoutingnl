<?php

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\custom_subsite\Entity\Subsite;
use Drupal\custom_subsite\Hook\ContentEditHooks;
use Drupal\custom_subsite\Hook\GroupSyncActions;
use Drupal\group\Entity\GroupInterface;
use Drupal\menu_link_content\MenuLinkContentInterface;
use Drupal\system\Entity\Menu;
use Drupal\system\MenuInterface;

/**
 * Implements hook_entity_bundle_info_alter().
 */
function custom_subsite_entity_bundle_info_alter(array &$bundles): void {
  if (isset($bundles['group']['subsite'])) {
    $bundles['group']['subsite']['class'] = Subsite::class;
  }
}

/**
 * Implements hook_form_alter().
 */
function custom_subsite_form_alter(array &$form, FormStateInterface $form_state, string $form_id): void {
  if (isset($form['subsite'])) {
    \Drupal::service(ContentEditHooks::class)->alterFieldableEntityForm($form, $form_state);
  }
}

/**
 * Implements hook_ENTITY_TYPE_access().
 */
function custom_subsite_menu_access(MenuInterface $entity, ?string $operation, AccountInterface $account): AccessResult {
  if ($operation !== 'update') {
    return AccessResult::neutral()->setCacheMaxAge(0);
  }

  return AccessResult::forbiddenIf(
    in_array($entity->id(), GroupSyncActions::MENU_IDS)
  )->setCacheMaxAge(0);
}

/**
 * Implements hook_ENTITY_TYPE_access().
 */
function custom_subsite_menu_link_content_access(MenuLinkContentInterface $menuLinkContent, ?string $operation, AccountInterface $account): AccessResult {
  if ($operation === 'update') {
    $current_subsite_id = \Drupal::service('custom_subsite.subsite_repository')
      ->getCurrentSubsiteFor($account)
      ->id();

    $parent_id = $menuLinkContent->get('parent')->value;
    assert(is_string($parent_id));
    if (mb_strpos($parent_id, 'custom_subsite.subsites') === FALSE) {
      return AccessResult::neutral()->setCacheMaxAge(0);
    }
    $parent_subsite_id = explode(':', $parent_id)[1];

    return AccessResult::forbiddenIf($current_subsite_id !== $parent_subsite_id)
      ->setCacheMaxAge(0);
  }
  return AccessResult::neutral();
}

/**
 * Implements hook_entity_operation_alter().
 */
function custom_subsite_entity_operation_alter(array &$operations, EntityInterface $entity): void {
  if ($entity instanceof Menu
    && in_array($entity->id(), GroupSyncActions::MENU_IDS)) {
    $operations = [
      'edit' => [
        'title' => t('Edit menu for subsite'),
        'weight' => 0,
        'url' => Url::fromRoute('custom_subsite.entity.menu.edit_form', ['menu' => $entity->id()]),
      ],
    ];
  }
}

/**
 * Implements hook_ENTITY_TYPE_insert().
 */
function custom_subsite_group_insert(GroupInterface $group): void {
  \Drupal::service(GroupSyncActions::class)->onGroupCreate($group);
}

function custom_subsite_group_delete(GroupInterface $group): void {
  \Drupal::service(GroupSyncActions::class)->onGroupDelete($group);
}

function custom_subsite_group_update(GroupInterface $group): void {
  \Drupal::service(GroupSyncActions::class)->onGroupAlter($group);
}
