<?php

/**
 * Implements hook_views_data_alter().
 */
function custom_subsite_views_data_alter(array &$data): void {
  $data['node_field_data']['subsite'] = [
    'title' => t('Subsite admin or subsite equals to own'),
    'help' => t('Own subsite or subset of.'),
    'filter' => [
      'id' => 'subsite_own',
    ],
  ];
}
