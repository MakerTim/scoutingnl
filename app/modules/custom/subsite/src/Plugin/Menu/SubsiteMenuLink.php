<?php

namespace Drupal\custom_subsite\Plugin\Menu;

use Drupal\Component\Render\MarkupInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Menu\MenuLinkBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\custom_subsite\SubsiteInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class SubsiteMenuLink extends MenuLinkBase implements ContainerFactoryPluginInterface {

  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
    );
  }

  public function getTitle(): string|MarkupInterface {
    assert(is_array($this->pluginDefinition));
    return $this->pluginDefinition['title'];
  }

  public function getDescription() {
    assert(is_array($this->pluginDefinition));
    return (string) $this->pluginDefinition['description'];
  }

  public function isEnabled(): bool {
    if (!parent::isEnabled()) {
      return FALSE;
    }

    return $this->getEntity()?->isPublished() ?? FALSE;
  }

  public function updateLink(array $new_definition_values, $persist): array {
    assert(is_array($this->pluginDefinition));
    return $this->pluginDefinition;
  }

  public function getOperations(): array {
    return [];
  }

  public function getCacheTags(): array {
    $entity_type = 'group';

    return Cache::mergeTags(parent::getCacheTags(), [
      sprintf('%s:%s', $entity_type, $this->getRouteParameters()[$entity_type]),
    ]);
  }

  protected function getEntity(): ?SubsiteInterface {
    $entity_type = 'group';

    $route_parameters = $this->getRouteParameters();
    assert(isset($route_parameters[$entity_type]));

    $entity_id = $route_parameters[$entity_type];
    assert(is_string($entity_id));

    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    assert($entity instanceof SubsiteInterface || $entity === NULL);
    return $entity;
  }

}
