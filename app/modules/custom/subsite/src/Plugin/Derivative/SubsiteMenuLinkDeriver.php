<?php

namespace Drupal\custom_subsite\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\custom_subsite\SubsiteRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class SubsiteMenuLinkDeriver extends DeriverBase implements ContainerDeriverInterface {

  public function __construct(
    protected readonly SubsiteRepository $subsiteRepository,
  ) {}

  public static function create(ContainerInterface $container, $base_plugin_id): self {
    return new self(
      $container->get('custom_subsite.subsite_repository'),
    );
  }

  public function getDerivativeDefinitions($base_plugin_definition): array {
    $links = [];

    foreach ($this->subsiteRepository->all() as $index => $subsite) {
      $url = $subsite->toUrl();

      $links[$subsite->id()] = [
        'title' => $subsite->label(),
        'route_name' => $url->getRouteName(),
        'route_parameters' => $url->getRouteParameters(),
        'weight' => $base_plugin_definition['weight'] + $index,
        'enabled' => $subsite->isPublished(),
      ] + $base_plugin_definition;
    }

    return $links;
  }

}
