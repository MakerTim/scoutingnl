<?php

namespace Drupal\custom_subsite\Plugin\views\filter;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Attribute\ViewsFilter;
use Drupal\views\Plugin\views\filter\FilterPluginBase;
use Drupal\views\Plugin\views\query\Sql;

#[ViewsFilter("subsite_own")]
final class Subsite extends FilterPluginBase {

  public function adminSummary(): string {
    return '';
  }

  protected function operatorForm(&$form, FormStateInterface $form_state): void {}

  public function canExpose(): bool {
    return FALSE;
  }

  public function query(): void {
    assert($this->query instanceof Sql);
    $query = $this->query;

    $join_table_alias = $query->queueTable('node__subsite', alias: 'node__subsite');
    $query->addWhereExpression($this->options['group'],
      "$join_table_alias.subsite_target_id LIKE '***CURRENT_SUBSITE***' OR ***SUBSITE_SUPERADMIN***=1");
  }

  public function getCacheContexts(): array {
    $contexts = parent::getCacheContexts();
    $contexts[] = 'user';
    return $contexts;
  }
}
