<?php

namespace Drupal\custom_subsite\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

final class RouteSubscriber extends RouteSubscriberBase {

  const ADMIN_ROUTES = [
    'entity.group.add_form' => TRUE,
    'entity.group.add_page' => TRUE,
    'entity.group.collection' => TRUE,
    'entity.group.edit_form' => TRUE,
    'entity.group.delete_form' => TRUE,
    'entity.group.version_history' => TRUE,
    'entity.group.revision' => TRUE,
    'entity.group.revision_revert_form' => TRUE,
    'entity.group.revision_delete_form' => TRUE,
    'view.group_members.page_1' => TRUE,
  ];

  protected function alterRoutes(RouteCollection $collection): void {
    if ($route = $collection->get('entity.menu.edit_form')) {
      $route->addRequirements([
        '_access', 'FALSE',
      ]);
    }
    foreach (self::ADMIN_ROUTES as $route => $is_admin_route_overwrite) {
      if ($route = $collection->get($route)) {
        $route->setOption('_admin_route', TRUE);
      }
    }
  }

}
