<?php

namespace Drupal\custom_subsite\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\Element;
use Drupal\custom_subsite\SubsiteRepository;
use Drupal\system\Entity\Menu;

final class SubsiteManagedMenulinkController extends ControllerBase {

  public function __construct(
    private readonly SubsiteRepository $subsiteRepository,
  ) {}

  public function editForm(Menu $menu): array {
    $form = $this->entityFormBuilder()->getForm($menu, 'edit');

    $subsite_id = $this->subsiteRepository
      ->getCurrentSubsiteFor($this->currentUser())
      ->id();
    $ids = Element::children($form['links']['links']);

    foreach ($ids as $id) {
      // Find the top-level menu item its subsite menu item.
      $root_id = $id;
      while ($root_id && mb_strpos($root_id, 'custom_subsite.subsites') === FALSE) {
        $item = $form['links']['links'][$root_id];
        $root_id = $item['parent']['#default_value'] ?? NULL;
        if ($root_id) {
          $root_id = 'menu_plugin_id:' . $root_id;
        }
      }
      if (mb_strpos($root_id, 'custom_subsite.subsites') !== FALSE) {
        $menu_item_subsite_id = explode(':', $root_id)[2];
        // Hide / remove links from other subsites.
        if ($subsite_id !== $menu_item_subsite_id) {
          unset($form['links']['links'][$id]);
        }
      }

      if (mb_strpos($id, 'custom_subsite.subsites') !== FALSE) {
        $menu_item_subsite_id = explode(':', $id)[2];
        // Hide / remove links from other subsites.
        if ($subsite_id === $menu_item_subsite_id) {
          $form['links']['links'][$id]['enabled']['#disabled'] = TRUE;
          $form['links']['links'][$id]['weight']['#disabled'] = TRUE;
          // $form['links']['links'][$id]['operations'] = [];
        }
      }
    }
    return $form;
  }

}
