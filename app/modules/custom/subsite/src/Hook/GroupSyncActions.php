<?php

namespace Drupal\custom_subsite\Hook;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\group\Entity\GroupInterface;

final class GroupSyncActions implements ContainerInjectionInterface {
  use AutowireTrait;

  const array MENU_IDS = [
    'main',
    'footer',
  ];

  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  public function onGroupCreate(GroupInterface $group): void {
    $this->entityTypeManager->getStorage($group->getEntityTypeId());
  }

  public function onGroupAlter(GroupInterface $group): void {
    $this->entityTypeManager->getStorage($group->getEntityTypeId());
  }

  public function onGroupDelete(GroupInterface $group): void {
    $this->entityTypeManager->getStorage($group->getEntityTypeId());
  }

}
