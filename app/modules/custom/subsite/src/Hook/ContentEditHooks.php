<?php

namespace Drupal\custom_subsite\Hook;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\custom_subsite\SubsiteRepository;

final class ContentEditHooks implements ContainerInjectionInterface {
  use AutowireTrait;

  public function __construct(
    private readonly AccountInterface $currentUser,
    private readonly SubsiteRepository $subsiteRepository,
  ) {}

  public function alterFieldableEntityForm(array &$form, FormStateInterface $form_state): void {
    $subsite = $this->subsiteRepository->getCurrentSubsiteFor($this->currentUser);

    $form['subsite']['#default_value'] ??= $subsite;
    $form['subsite']['#disabled'] = !$this->currentUser->hasPermission('manager of subsites');
    $form['subsite']['widget'][0]['target_id']['#default_value'] ??= $subsite;
  }

}
