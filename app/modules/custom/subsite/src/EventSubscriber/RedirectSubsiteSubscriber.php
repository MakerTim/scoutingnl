<?php

namespace Drupal\custom_subsite\EventSubscriber;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\custom_subsite\Hook\GroupSyncActions;
use Drupal\custom_subsite\SubsiteInterface;
use Drupal\system\MenuInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

final class RedirectSubsiteSubscriber implements EventSubscriberInterface {

  public function __construct(
    private readonly RouteMatchInterface $routeMatch,
    private readonly AccountInterface $currentUser,
  ) {}

  public function onRequest(RequestEvent $event): void {
    $current_route = $this->routeMatch->getRouteName();
    if ($current_route === 'entity.menu.edit_form'
    && $menu = $this->routeMatch->getParameter('menu')) {
      assert($menu instanceof MenuInterface);

      if (in_array($menu->id(), GroupSyncActions::MENU_IDS)
      && !$this->currentUser->hasPermission('super manage subsite-menu')) {
        $response = $this->createRedirectResponse(
          Url::fromRoute('custom_subsite.entity.menu.edit_form')->setRouteParameter('menu', $menu->id())
        );
        $response->addCacheableDependency((new CacheableMetadata())->addCacheContexts(['user.permissions']));
        $event->setResponse($response);
        return;
      }
    }

    if (in_array($current_route, [
      'entity.group.canonical',
      'entity.group.revision',
      'entity.group.version_history',
    ]) && $subsite = $this->routeMatch->getParameter('group')) {
      assert($subsite instanceof SubsiteInterface);

      $response = $this->createRedirectResponse($subsite->getFront()->toUrl());
      $response->addCacheableDependency($subsite);

      $event->setResponse($response);
      return;
    }
  }

  private function createRedirectResponse(Url $url): Response&CacheableResponseInterface {
    $generated_url = $url->toString(TRUE);

    $response = new TrustedRedirectResponse($generated_url->getGeneratedUrl(), 303);
    $response->addCacheableDependency($generated_url);
    return $response;
  }

  /**
   * @see \Symfony\Component\HttpKernel\EventListener\RouterListener::getSubscribedEvents()
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::REQUEST][] = ['onRequest', 31];
    return $events;
  }

}
