<?php

namespace Drupal\custom_subsite\Entity;

use Drupal\custom_subsite\SubsiteInterface;
use Drupal\group\Entity\Group;
use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;

final class Subsite extends Group implements SubsiteInterface {

  public function id(): ?string {
    /** @var ?string */
    return $this->getEntityKey('id');
  }

  public function getSiteName(): string {
    /** @var string */
    return $this->getEntityKey('label');
  }

  public function getDomains(): array {
    /** @var string[] */
    return $this->get('subsite__domains')->value;
  }

  public function getWeight(): float|int|string {
    /** @var float|int|string */
    return $this->get('subsite__weight')->value;
  }

  public function getIcon(): MediaInterface {
    /** @var \Drupal\media\MediaInterface */
    return $this->get('subsite__icon')->entity;
  }

  public function getFront(): NodeInterface {
    /** @var \Drupal\node\NodeInterface */
    return $this->get('subsite__front')->entity;
  }

}
