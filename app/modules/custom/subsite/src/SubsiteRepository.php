<?php

namespace Drupal\custom_subsite;

use Drupal\Core\DependencyInjection\AutowireTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\group\Entity\GroupRelationshipInterface;

final class SubsiteRepository implements ContainerInjectionInterface {
  use AutowireTrait;

  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
  ) {}

  /**
   * @return \Drupal\custom_subsite\SubsiteInterface[]
   */
  public function all(): array {
    $group_storage = $this->entityTypeManager->getStorage('group');
    $query = $group_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', 1)
      ->sort('subsite__weight');
    /** @var \Drupal\custom_subsite\SubsiteInterface[] */
    return $group_storage->loadMultiple($query->execute());
  }

  /**
   * @return \Drupal\custom_subsite\SubsiteInterface[]
   */
  public function getAllSubsitesFor(AccountInterface $user): array {
    $group_relationship_storage = $this->entityTypeManager->getStorage('group_relationship');
    $query = $group_relationship_storage->getQuery()
      ->accessCheck(FALSE)
      ->condition('entity_id', $user->id())
      ->condition('group_type', 'subsite')
      ->sort('created');
    /** @var \Drupal\group\Entity\GroupRelationshipInterface[] $group_memberships */
    $group_memberships = $group_relationship_storage->loadMultiple($query->execute());
    /** @var \Drupal\custom_subsite\SubsiteInterface[] */
    return array_map(
      fn (GroupRelationshipInterface $group_relationship) => $group_relationship->getGroup(),
      $group_memberships
    );
  }

  public function getCurrentSubsiteFor(AccountInterface $user): SubsiteInterface {
    $subsites = $this->getAllSubsitesFor($user);
    // @todo better way of selecting subsite.
    $subsite = reset($subsites);
    if (!$subsite instanceof SubsiteInterface) {
      throw new \RuntimeException($user->id() . ' has no subsite.');
    }
    return $subsite;
  }

}
