<?php

namespace Drupal\custom_subsite;

use Drupal\Core\Entity\EntityInterface;
use Drupal\media\MediaInterface;
use Drupal\node\NodeInterface;

interface SubsiteInterface extends EntityInterface {

  public function id(): ?string;

  public function getSiteName(): string;

  /**
   * @return string[]
   */
  public function getDomains(): array;

  public function getWeight(): float|int|string;

  public function getIcon(): MediaInterface;

  public function getFront(): NodeInterface;

  /**
   * @return bool
   */
  public function isPublished();

}
